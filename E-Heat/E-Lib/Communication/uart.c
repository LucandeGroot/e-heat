/*
 * uart.c
 *
 *  Created on: 13 dec. 2017
 *      Author: lucan
 */
#include "uart.h"

#include "em_usart.h"
#include "em_leuart.h"
#include "em_gpio.h"
#include "em_cmu.h"

#include <stdio.h>	// sprintf
#include <math.h>   // for log

// we can use a really fance mathmetic function to calculate this
// but that would be slower and this is easier
uint8_t number_of_digits_in_int(uint32_t n) {
	    if (n < 10) return 1;
	    if (n < 100) return 2;
	    if (n < 1000) return 3;
	    if (n < 10000) return 4;
	    if (n < 100000) return 5;
	    if (n < 1000000) return 6;
	    if (n < 10000000) return 7;
	    if (n < 100000000) return 8;
	    if (n < 1000000000) return 9;
	    /*      2147483647 is 2^31-1 - add more ifs as needed
	    and adjust this final return as well. */
	    return 10;
}

// plot one value in SerialPortPlotter
void plot1(uint32_t val1) {
	uint8_t total_message[number_of_digits_in_int(val1) + 3]; // size of the number + 3 for the extra chars

	// format the numbers into the string
	sprintf((char*)total_message, "$%d;", (int)val1);

	transmit_string(total_message, sizeof(total_message), NEWLINE_FALSE);
}

// plot two values in SerialPortPlotter
void plot2(uint32_t val1, uint32_t val2){
	uint8_t total_message[number_of_digits_in_int(val1) +
						  number_of_digits_in_int(val2) + 3]; // size of the two numbers + 3 for the extra chars

	// format the numbers into the string
	sprintf((char*)total_message, "$%d %d;", (int)val1, (int)val2);

	transmit_string(total_message, sizeof(total_message), NEWLINE_FALSE);
}

// TODO: FIX this
void transmit_int(int number, uint8_t newline){
	//uint8_t data_buffer[(int)(log(number)+2)]; // max int size is 12 char

	//sprintf((char*)data_buffer, "i", number);

	//transmit_string(data_buffer, (int)(log(number)+2), newline);
}

// transmit a string
void transmit_string(uint8_t* string_to_send, uint8_t length, uint8_t newline){

	while(*string_to_send != 0x00){
		transmit_char(string_to_send++);
	}

	// check if the user wanted a newline, if so print a newline
	if(newline){
		transmit_char((uint8_t*)"\r");
		transmit_char((uint8_t*)"\n");
	}
}

// function to send a char
void transmit_char(uint8_t* char_to_send){
#ifdef LE_UART
	LEUART_Tx(LEUART0, *char_to_send);
#elif UART
	USART_Tx(USART0, *char_to_send);
#endif
}

void init_uart(){
#if defined(DEBUG) || defined(DATA_AGGREGATION)
	CMU_ClockEnable(cmuClock_LEUART0, true);
	CMU_ClockEnable(cmuClock_LFB, true);
	CMU_ClockEnable(cmuClock_HFLE, true);
	CMU_ClockSelectSet(cmuClock_LFB, cmuSelect_LFXO);

	// Set the TX pin to push-pull output with pull-up
	GPIO_PinModeSet(TX_PORT, TX_PIN, gpioModePushPull, 1);
#endif

#ifdef LE_UART
	// start the leuart clock
	CMU_ClockEnable(cmuClock_LEUART0, true);

	/* Default config:
	 *
	 * Enable after init
	 * Current reference clock
	 * 9600 baud
	 * 8 data bits
	 * No parity
	 * 1 Stopbit
	 */
	LEUART_Init_TypeDef leuartinit = LEUART_INIT_DEFAULT;



	// set the correct pin input (PD15)
	// see Table 6.7 Alternate functionality overview in the efr32bg1-datasheet (Page 120)
	// https://www.silabs.com/documents/login/data-sheets/efr32bg1-datasheet.pdf
	//LEUART0->ROUTELOC0 = (LEUART0->ROUTELOC0 & (~_LEUART_ROUTELOC0_TXLOC_MASK)) | LEUART_ROUTELOC0_TXLOC_LOC23;
	LEUART0->ROUTELOC0 = (LEUART0->ROUTELOC0 & (~_LEUART_ROUTELOC0_TXLOC_MASK)) | LEUART_ROUTELOC0_TXLOC_LOC0;

	// enable the TX pen
	LEUART0->ROUTEPEN = LEUART0->ROUTEPEN | LEUART_ROUTEPEN_TXPEN;

	// apply the config
	LEUART_Init(LEUART0, &leuartinit);

	LEUART_Enable(LEUART0, leuartEnable );
#elif UART
	// start the uart clock
	CMU_ClockEnable(cmuClock_USART0, true);

	/* Default config:
	 *
	 * Enable after init
	 * Current reference clock
	 * 115200 baud
	 * 16x oversampling
	 * 8 data bits
	 * No parity
	 * 1 stop bit
	 */
	USART_InitAsync_TypeDef uartinit = USART_INITASYNC_DEFAULT;

	// Make sure no flowcontrol
	uartinit.hwFlowControl = false;

	// send the configuration
	USART_InitAsync(USART0, &uartinit);

	// set the correct pin input (PD15)
	// see Table 6.7 Alternate functionality overview in the efr32bg1-datasheet (Page 120)
	// https://www.silabs.com/documents/login/data-sheets/efr32bg1-datasheet.pdf
	USART0->ROUTELOC0 = (USART0->ROUTELOC0 & (~_USART_ROUTELOC0_TXLOC_MASK)) | USART_ROUTELOC0_TXLOC_LOC23;

	// enable the TX pen
	USART0->ROUTEPEN = USART0->ROUTEPEN | USART_ROUTEPEN_TXPEN;
#endif

	//transmit_string((uint8_t*)"Uart init done!",sizeof((uint8_t*)"Uart init done!"),true);
}

