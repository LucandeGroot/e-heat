/*
 * debug.h
 *
 *  Created on: 22 dec. 2017
 *      Author: lucan
 */

#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdint.h>  // for uint
#include <bsp_trace.h> // for debugging with SWO
#include "uart.h"


// comment if you do not want to debug (so make a release version)
#define DEBUG 1
#define DATA_AGGREGATION 1


// debug message macro
#ifndef DEBUG
#define DEBUG_MESSAGE(val1, val2, val3) __noop()// don't do anything with the data // OLD, DO NOT USE
#define DEBUG_SIMPLE(message,newline)	// don't do anything with the data
#elif DEBUG
#define UART 1
#define DEBUG_MESSAGE(message, length, newline) transmit_string(message, length, newline) // OLD, DO NOT USE
#define DEBUG_SIMPLE(message,newline) transmit_string(message,0,newline)
#endif

void enable_debug();
uint8_t check_for_kit();

#endif /* DEBUG_H_ */
