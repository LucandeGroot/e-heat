/*
 * bluetooth.h
 *
 *  Created on: 5 dec. 2017
 *      Author: lucan
 */

#ifndef BLUETOOTH_H_
#define BLUETOOTH_H_

#include <stdint.h>
#include "native_gecko.h"
#include "debug.h"
#include "Data Aggregation/Data_aggregation.h"
#include "TDC1000_defs.h"


#define DATA_AGGREGATION_PRODUCT_NAME 						"DGAS"
#define DATA_AGGREGATION_NUM_TOF_IN_PACKET					(8)
#define DATA_AGGREGATION_MESSAGES_PER_INTERVAL				(2)
#define DATA_AGGREGATION_ADVERTISEMENT_INTERVAL				(0x20)
#define DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT 	(TDC1000_MAX_STOP / DATA_AGGREGATION_NUM_TOF_IN_PACKET) // How many packets do we need to send all the data
#define DATA_AGGREGATION_BLUETOOTH_TX_TIME					((DATA_AGGREGATION_ADVERTISEMENT_INTERVAL * 1.6) * 1000.0)

#define PRODUCT_NAME  "EGAS"

#define NAME_MAX_LENGTH 4				// JOCK

#define MAX_READINGS_IN_MESSAGE 3		// amount of messages in the message_struct
#define MAX_MTU_SIZE 31					// max size of the advertisement message

#define MAX_ADVERTISEMENT_PER_READING 1 // max advertisement per reading ==> reading refreshed every # advertisements
#define ADVERTISEMENT_INTERVAL 0x20 	// The min interval

#define SETTINGS_MESSAGES_PER_INTERVAL_SLOW 2
//#define COMPANY_ID 0xABCD				// The id of E-Trailer in the message data




extern uint8_t bluetooth_done;
extern uint8_t reading_counter;

/* Struct to send the data
 *
 * Max 31 bytes
 */
typedef struct {

	// BLE flags
	uint8_t len_flags;
	uint8_t type_flags;
	uint8_t val_flags;

	/* --- 3 Bytes used --- */

	// information about the length of the next two bytes
	uint8_t len_manuf;	// length of the rest of the message
	uint8_t type_manuf;	// 0xFF : MANUFACTURER SPECIFIC DATA

	/* --- 5 Bytes used --- */

	/* Name is 4 characters we could change this to a 2 char value,
	 * this could give us the space to add one more reading.
	 */
	char name[NAME_MAX_LENGTH];    // NAME_MAX_LENGTH must be sized so that total length of data does not exceed 31 bytes

	/* --- 9 bytes used --- */

	uint8_t message_counter;

	/* --- 10 bytes used --- */

	// 0..9 readings
	uint16_t readings[MAX_READINGS_IN_MESSAGE];    // 10 * 2 = 20 + 10 = 30 Bytes

	/* --- 30 bytes used --- */
	// one byte left

	/* these values are NOT included in the actual advertising payload, just for bookkeeping */

	char dummy;    // space for null terminator
	uint8_t data_size;    // actual length of advertising data may not exceed 31 byes
} message_struct_typedef;

/* Struct to send all TOF
 *
 * Max 31 bytes
 */
typedef struct __attribute__ ((packed)) {

	// BLE flags
	uint8_t len_flags;
	uint8_t type_flags;
	uint8_t val_flags;

	/* --- 3 Bytes used --- */

	// information about the length of the next two bytes
	uint8_t len_manuf;	// length of the rest of the message
	uint8_t type_manuf;	// 0xFF : MANUFACTURER SPECIFIC DATA

	/* --- 5 Bytes used --- */

	char name[NAME_MAX_LENGTH];

	/* --- 9 bytes used --- */

	uint8_t temp;
	uint16_t measurement_number;
	uint8_t treshold;
	uint8_t num_tx_pulse;
	uint8_t tx_pulse_shift;

	/* --- 15 bytes used --- */

	uint16_t TOF[DATA_AGGREGATION_NUM_TOF_IN_PACKET];

	/* --- 31 bytes used --- */


	/* these values are NOT included in the actual advertising payload, just for bookkeeping */

	char dummy;    // space for null terminator
	uint8_t data_size;    // actual length of advertising data may not exceed 31 byes
} message_struct_data_aggregation_t;

// the struct that contains the message
extern message_struct_typedef message_struct;

void init_bluetooth();
void init_bluetooth_low_energy( void );
void init_bluetooth_data_aggregation( message_struct_data_aggregation_t packets[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT] );

void fill_adv_packet(message_struct_typedef *, uint8_t, char *);
void start_adv(message_struct_typedef *);
void update_adv_data(message_struct_typedef *);

void add_reading(message_struct_typedef *, uint16_t, uint8_t index);
void remove_readings(message_struct_typedef *);

void print_mac_at_startup();

/*
 *  /description fill the packet to send over Bluetooth.
 */
void update_packet_to_send(uint16_t * values,uint8_t amount);

void start_sending(void);
void stop_sending(void);

#endif /* BLUETOOTH_H_ */
