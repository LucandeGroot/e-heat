/*
 * uart.h
 *
 *  Created on: 13 dec. 2017
 *      Author: lucan
 */

#ifndef UART_H_
#define UART_H_


#include <stdint.h>
#include "debug.h"


// comment if you don't want to use uart
#ifdef UART
#define LE_UART 1 	// comment if you don't want to use le uart
					// normal uart does not work (properly!) in EM2!
#endif

// uart TX pin
// if you change this pin, also change the pin route register in the init
// TODO: Change pin location to a non adc bus.
//#define TX_PORT gpioPortD
//#define TX_PIN	15
#define TX_PORT gpioPortA
#define TX_PIN	0

// newline defines
#define NEWLINE_TRUE 1
#define NEWLINE_FALSE 0

// functions
void init_uart();
void transmit_char(uint8_t*);
void transmit_string(uint8_t*, uint8_t, uint8_t);

void plot1(uint32_t);
void plot2(uint32_t, uint32_t);
void transmit_int(int, uint8_t);
uint8_t number_of_digits_in_int(uint32_t);

#endif /* UART_H_ */
