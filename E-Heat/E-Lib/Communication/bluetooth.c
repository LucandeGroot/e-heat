/*
 * bluetooth.c
 *
 *  Created on: 5 dec. 2017
 *      Author: lucan
 */

#include "bluetooth.h"


#include <stdio.h> 		// for sprintf
#include <inttypes.h> 	// for sprintf with uint32_t
#include <measurement.h>		// needed for product name and readings per bluetooth
#include "uart.h"		// for transmit int
#include "Settings_config.h"
#include "../GPIO/dcdc.h"
#include "Data Aggregation/Data_aggregation.h"

// counter to keep check of the readings
uint8_t reading_counter = 0;
message_struct_typedef message_struct;
uint8_t non_connectable_mode = 0;
uint8_t bluetooth_done = 0;

static void print_packet(void);


void init_bluetooth_data_aggregation(message_struct_data_aggregation_t packets[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT]){
//	// Set the correct bluetooth paramenter
//    gecko_cmd_le_gap_set_adv_parameters(DATA_AGGREGATION_ADVERTISEMENT_INTERVAL, DATA_AGGREGATION_ADVERTISEMENT_INTERVAL, 7);
//
//    // How many times we want to send the same data
//    gecko_cmd_le_gap_set_adv_timeout (DATA_AGGREGATION_MESSAGES_PER_INTERVAL);
//
//    // Not able to connect and send user data
//    gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_non_connectable);

    // Initialize the packets to send
    for (uint8_t i = 0; i < DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT; i++) {

    	// Set the correct flags
    	packets[i].len_flags = 0x02;
    	packets[i].type_flags = 0x01;    // AD Type: say to bluetooth that the next byte are flags
    	packets[i].val_flags = 0x06;

    	// We are going to send manufacture data
    	packets[i].len_manuf =	1 + NAME_MAX_LENGTH + (DATA_AGGREGATION_NUM_TOF_IN_PACKET * sizeof(packets[i].TOF[0]) );
		packets[i].type_manuf = 0xFF; // send user data

    	// Copy the name into the struct
    	strncpy(packets[i].name, DATA_AGGREGATION_PRODUCT_NAME, NAME_MAX_LENGTH);

    	// Make sure the dummy byte is 0
    	packets[i].dummy = 0x00;
    }
}

void init_bluetooth() {

	// send one advertisement
	// TODO: update to 2.9.2.0
    gecko_cmd_le_gap_set_adv_parameters(ADVERTISEMENT_INTERVAL, ADVERTISEMENT_INTERVAL, 7);

    gecko_cmd_le_gap_set_adv_timeout (SETTINGS_MESSAGES_PER_INTERVAL_SLOW);


    /* Start general advertising and enable connections. */
    gecko_cmd_le_gap_set_mode(le_gap_general_discoverable, le_gap_undirected_connectable);

	/* See for more info about flags:
	 *
	 * Bluetooth Core Specification:
	 * Vol. 3, Part C, section 8.1.3 (v2.1 + EDR, 3.0 + HS and 4.0)
	 * Vol. 3, Part C, sections 11.1.3 and 18.1 (v4.0)
	 * Core Specification Supplement, Part A, section 1.3
	 *
	 * 0x06 = 0000 0110 which means:
	 * 	LE General Discoverable Mode
	 * 	BR/EDR Not supported
	 *
	 * https://www.bluetooth.com/specifications/bluetooth-core-specification/legacy-specifications
	 */
	// TODO: Figure out what companyID we are going to use
	fill_adv_packet(&message_struct, 0x06, PRODUCT_NAME);

	// make sure all the readings are cleared and the message counter is reset
	remove_readings(&message_struct);
	//message_structp->message_counter = 0;

	non_connectable_mode = 0;
}

void init_bluetooth_low_energy( void )
{
	//gecko_cmd_le_gap_set_adv_timeout (MAX_ADVERTISEMENT_PER_READING); //no need for this
	gecko_cmd_le_gap_set_adv_parameters(ADVERTISEMENT_INTERVAL, ADVERTISEMENT_INTERVAL, 7);
	non_connectable_mode = 1;
}

/*
 * Function to fill the advertisement package
 */
void fill_adv_packet(message_struct_typedef *message_structp, uint8_t flags,  char *name) {
	int n;

	message_structp->len_flags = 0x02;
	message_structp->type_flags = 0x01;    // AD Type: say to bluetooth that the next byte are flags
	message_structp->val_flags = flags;

	// copy the first 4 chars of the given name into the struct
	strncpy(message_structp->name, name, NAME_MAX_LENGTH);

	n = strlen(name);    // name length, excluding the null terminator
	// for size calculation purposes
	if (n > NAME_MAX_LENGTH) {
		n = NAME_MAX_LENGTH;
	}

	/* Calculate the length of the manufacturer data
	 * 1 (AD Type)
	 * The name (n)
	 * the readings (2 Bytes per reading)
	 */
	message_structp->len_manuf = 1 + n + (MAX_READINGS_IN_MESSAGE*2);
	message_structp->type_manuf = 0xFF;    // AD Type: User defined data

	/* calculate total length of advertising data
	 * The flags (3)
	 * The user_data (2 + name + readings)
	 */
	message_structp->data_size = (1 + message_structp->len_flags) + (2 + message_structp->len_manuf);

	print_packet();
}

void start_adv(message_struct_typedef *message_struct) {
	// set custom advertising payload
	gecko_cmd_le_gap_set_adv_data(0, message_struct->data_size, (const uint8_t *) message_struct);

	/* start advertising using custom data */
	if (non_connectable_mode == 1)
	{
		gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_non_connectable);
	}
#ifdef DEBUG
	char hexdata[50] = {0};
	sprintf(hexdata, "[%d], [%d], [%d]", message_struct->readings[0], message_struct->readings[1], message_struct->readings[2]);
	DEBUG_SIMPLE((uint8_t*) hexdata,  NEWLINE_TRUE);
#endif
	// increase the message_counter
	message_struct->message_counter++;

	print_packet();
}

void update_adv_data(message_struct_typedef *message_struct) {

	//set custom advertising payload
	//gecko_cmd_le_gap_set_adv_data(0, message_struct->data_size, (const uint8_t *) message_struct);
	uint8_t handle = 0;
	gecko_cmd_le_gap_bt5_set_adv_data(handle,0, message_struct->data_size, (const uint8_t *) message_struct);

	if (non_connectable_mode == 1)
	{
		gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_non_connectable);
	}
	// increase the message_counter
	message_struct->message_counter++;

	reading_counter = 0;


}

void add_reading(message_struct_typedef *message_struct, uint16_t reading,uint8_t index) {
	// add the reading and increase the index
	message_struct->readings[index] = reading;

	reading_counter++;

	// if we've reached the end of the readings array... start at the beginning
	/*if (reading_counter >= MAX_READINGS_IN_MESSAGE) {
		reading_counter = 0;
		update_adv_data(message_struct);
	}*/
}

void remove_readings(message_struct_typedef *message_struct) {
// remove all readings
	for (uint8_t i = 0; i < MAX_READINGS_IN_MESSAGE; i++) {
		message_struct->readings[i] = 0;

	}
}


void update_packet_to_send(uint16_t * values,uint8_t amount)
{

	remove_readings(&message_struct);
	for (uint8_t i = 0; i < amount; i++)
	{
		add_reading(&message_struct,*(values + i),i);
	}
}

void start_sending(void)
{

	// Make sure the radio can get enough power
	enable_radio();

	//start sending
	start_adv(&message_struct);
	gecko_cmd_le_gap_set_mode(le_gap_user_data, le_gap_undirected_connectable);

}

void stop_sending(void)
{
	// Save some power
	disable_radio();

	//stop_sending
	gecko_cmd_le_gap_set_mode(le_gap_non_discoverable, le_gap_non_connectable);
}


void print_packet()
{
#ifdef DEBUG
	/*char hex[30] = {0};
	for (uint8_t i = 0; i < MAX_MTU_SIZE; i++)
	{
		uint8_t *pointer = &message_struct;
		sprintf(hex," %.2x", *(pointer + i));
		DEBUG_SIMPLE(hex,NEWLINE_FALSE);
	}*/
#endif
}
