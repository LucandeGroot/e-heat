/*
 * Decision_making.c
 *
 *  Created on: 21 aug. 2018
 *      Author: Koen_E-Trailer
 */

#ifndef DECISION_MAKING_H_
#define DECISION_MAKING_H_

#include <inttypes.h> // for uint

typedef enum {
	ST_SLEEPING,
	ST_START_MEASURING,
	ST_MEASURING,
	ST_START_SENDING,
	ST_BT_SENDING,
	ST_ANALYSE_MEASUREMENTS,
	ST_DEEP_SLEEPING
} E_GAS_STATES_t;


/*
 *  /description  Init for the decision_making main
 * 	/return -1 if error
 * 	/return 0 if no error
 */
int8_t Decision_making_init(void);

/*
 * 	/description This is the main for decision making.
 * 	/return -1 if error
 * 	/return 0 if no error
 */
int8_t Decision_making(void);

/*
 *  /description  function for extern change of state_machine
 * 	/return -1 if not allowed
 * 	/return 0 if allowed and succesfull
 */
int8_t Decision_change(E_GAS_STATES_t input);



#endif
