/*
 * Data_aggregation.c
 *
 *  Created on: 27 okt. 2018
 *      Author: lucan
 */
#include "Data_aggregation.h"
#include "bluetooth.h"
#include "TDC1000.h"
#include "TDC1000_defs.h"
#include "measurement.h"
#include "stopwatch.h"
#include "GPIO/dcdc.h"
#include "sleep_mode.h"
#include "GPIO/externalclock.h"

static void data_aggregation_get_TOF(uint16_t TOF_array[]);
static void data_aggregation_update_bt_packets(	message_struct_data_aggregation_t packets[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT],
												uint16_t TOF_array[MAX_STOPS],
												uint8_t treshold,
												uint8_t num_tx,
												uint8_t temp,
												uint8_t tx_phase_shift,
												uint16_t measurement_number);
static void data_aggregation_send_data_over_bt( message_struct_data_aggregation_t data_to_send[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT]);
static void data_aggregation_send_data_over_uart( message_struct_data_aggregation_t data_to_send[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT]);
static void clear_bt_packets(message_struct_data_aggregation_t data_to_send[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT]);


void data_aggregatoon_init() {
	// Initialize the TDC1000
	TDC1000_init(TDC1000_MASTER, TDC1000_BAUD);
}

void data_aggregation_start_measurement() {
	static uint8_t treshold = TDC1000_ECHO_THRESHOLD_35_mV, num_tx = TDC1000_TX_1_PULSE, TDC1000_freq = 0;
	static uint16_t measurement_number = 0;
	uint8_t temperature;

	message_struct_data_aggregation_t packets[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT];
	uint16_t TOF_readings[MAX_STOPS] = {0};

	// Loop through all the different combinations
	for (treshold = 0; treshold <= TDC1000_ECHO_THRESHOLD_440_mV; treshold++) {
		// Update the TDC1000 registers
		TDC1000_Registers.CONFIG_3.value |= (treshold << TDC1000_ECHO_THRESHOLD_OFFSET);
		TDC1000_WriteWithCheck(&TDC1000_Registers.CONFIG_3);

		for(num_tx = 1; num_tx <= TDC1000_TX_3_PULSE; num_tx++){

			// Update the TDC1000 registers
			TDC1000_Registers.CONFIG_0.value |= (num_tx << TDC1000_NUM_TX_OFFSET);
			TDC1000_WriteWithCheck(&TDC1000_Registers.CONFIG_0);

			for(TDC1000_freq = 0; TDC1000_freq < 4; TDC1000_freq++){
				if(TDC1000_freq ==0){
					external_clock_set_150_KHz();
				} else if (TDC1000_freq == 1) {
					external_clock_set_210_KHz();
				} else if (TDC1000_freq == 2) {
					external_clock_set_420_KHz();
				} else if (TDC1000_freq == 3) {
					external_clock_set_840_KHz();
				}

				// Increase the measurement
				measurement_number++;

				// Get the temperature
				temperature = (uint8_t)TDC1000_get_temp();


				// Start measurement
				start_measurement();


				// Wait for timeout
				wait_for_stopwatch(MEASUREMENT_TIMEOUT_US);


				// Stop measurement
				stop_measurement();

				// Convert the data to TOF
				data_aggregation_get_TOF(TOF_readings);


				// Put the data into the BT struct
				data_aggregation_update_bt_packets(	packets,
													TOF_readings,
													treshold,
													num_tx,
													temperature,
													num_tx, // tx shift
													measurement_number);


				// Send the data over Bluetooth
				data_aggregation_send_data_over_bt(packets);

				// Send the data over UART
				data_aggregation_send_data_over_uart(packets);

				//for(uint32_t i = 0; i < 0xffff; i++) __NOP();
				}
		}
	}

	measurement_number = 0;
}

static void data_aggregation_get_TOF(uint16_t TOF_array[]){
	uint16_t start_time;

	// Reset array
	memset(TOF_array, 0x00, sizeof(uint16_t) * MAX_STOPS);

	// get the start time
	start_time = convert_struct_to_us(timings.start_timings[0]);

	// Get the TOF for every stop
	for(uint8_t i = 0; i < timings.stop_counts; i++){
		TOF_array[i] = convert_struct_to_us(timings.stop_timings[i]) - start_time;
	}
}


static void data_aggregation_update_bt_packets( message_struct_data_aggregation_t packets[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT],
												uint16_t TOF_array[MAX_STOPS],
												uint8_t treshold,
												uint8_t num_tx,
												uint8_t temp,
												uint8_t tx_pulse_shift,
												uint16_t measurement_number) {

	// Clear all the bluetooth packets
	clear_bt_packets(packets);

	// update the Bluetooth packets
	for(uint8_t i = 0; i < DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT; i++){
		packets[i].measurement_number = measurement_number;
		packets[i].treshold = treshold;
		packets[i].num_tx_pulse = num_tx;
		packets[i].temp = temp;
		packets[i].tx_pulse_shift = tx_pulse_shift;

		// Copy DATA_AGGREGATION_NUM_TOF_IN_PACKET amount of bytes to the array
		// i = 0 --> copy 0..8
		// i = 1 --> copy 9..17
		memcpy(packets[i].TOF, &TOF_array[i * DATA_AGGREGATION_NUM_TOF_IN_PACKET], sizeof(packets[i].TOF[0]) * DATA_AGGREGATION_NUM_TOF_IN_PACKET);

		// Insert the maximum size
		packets[i].data_size = sizeof(packets[i]);

		// Remove the size of the data we won't send
		packets[i].data_size -= sizeof(packets[i].dummy) + sizeof(packets[i].data_size);
	}
}


static void data_aggregation_send_data_over_bt(message_struct_data_aggregation_t data_to_send[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT]) {

	// Make sure the dcdc is configured correctly
	enable_radio();

	init_bluetooth_data_aggregation(data_to_send);

	// Clear all stack events
	// You really should not do this like this
	// This can break your code if you expect stack commando's
	while(gecko_peek_event());

	// Send data, but only when the first element of the struct has data
	for(uint8_t i = 0; i < DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT && data_to_send[i].TOF[0] > 0; i++) {

		// Set the advertisement data
		gecko_cmd_le_gap_set_adv_data(0, data_to_send[i].data_size, (const uint8_t *) &(data_to_send[i]));

		// Set the advertisement parameters
		gecko_cmd_le_gap_set_advertise_timing(0, 0x20, 0x21, 0, 1);

		// Set the advertisement settings
		gecko_cmd_le_gap_start_advertising(0, le_gap_user_data, le_gap_non_connectable);

		// Wait for timeout
		while(BGLIB_MSG_ID(gecko_peek_event()->header) != gecko_evt_le_gap_adv_timeout_id) EMU_EnterEM1();
	}

	// disable the advertisements
	gecko_cmd_le_gap_start_advertising(0, le_gap_non_discoverable, le_gap_non_connectable);

	// Save some power again
	stop_sending();

	// Delay a bit
	for(uint32_t i = 0; i < 0xFF; i++) __NOP();
}

static void data_aggregation_send_data_over_uart(message_struct_data_aggregation_t data_to_send[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT]) {

	// [temp][measurement_num][threshold][num_tx_pulse][tx_pulse_shift][TOF * 32]

	// temp %d
	// meas_num %d
	// threshold %d
	// num_tx_pulse %d
	// tx_pulse_shift %d
	// TOF %d * 32

	char hexdata[30] = {0};

	sprintf(hexdata," temp %d", data_to_send[0].temp);
	transmit_string(hexdata,0,0);

	sprintf(hexdata," meas_num %d", data_to_send[0].measurement_number);
	transmit_string(hexdata,0,0);

	sprintf(hexdata," threshold %d", data_to_send[0].treshold);
	transmit_string(hexdata,0,0);

	sprintf(hexdata," num_tx_pulse %d", data_to_send[0].num_tx_pulse);
	transmit_string(hexdata,0,0);

	sprintf(hexdata," tx_pulse_shift %d", data_to_send[0].tx_pulse_shift);
	transmit_string(hexdata,0,0);

	transmit_string(" TOF ",0,0);

	for(uint8_t i = 0; i < DATA_AGGREGATION_NUM_TOF_IN_PACKET; i++){
		sprintf(hexdata,"%d ", data_to_send[0].TOF[i]);
		transmit_string(hexdata,0,0);
	}
	for(uint8_t i = 0; i < DATA_AGGREGATION_NUM_TOF_IN_PACKET; i++){
		sprintf(hexdata,"%d ", data_to_send[1].TOF[i]);
		transmit_string(hexdata,0,0);
	}
	for(uint8_t i = 0; i < DATA_AGGREGATION_NUM_TOF_IN_PACKET; i++){
		sprintf(hexdata,"%d ", data_to_send[2].TOF[i]);
		transmit_string(hexdata,0,0);
	}
	for(uint8_t i = 0; i < DATA_AGGREGATION_NUM_TOF_IN_PACKET; i++){
		sprintf(hexdata,"%d ", data_to_send[3].TOF[i]);
		transmit_string(hexdata,0,0);
	}

	transmit_string(" \n ",0,0);

}

static void clear_bt_packets(message_struct_data_aggregation_t data_to_clear[DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT]){
	for(uint8_t i = 0; i < DATA_AGGREGATION_BLUETOOTH_PACKETS_PER_MEASUREMENT; i++){
		memset(data_to_clear->TOF, 0x00, sizeof(data_to_clear->TOF[0]) * DATA_AGGREGATION_NUM_TOF_IN_PACKET);
	}
}
