

#include "sleep_mode.h"
#include "em_emu.h"
#include "em_cmu.h"
#include "em_cryotimer.h"
#include "Communication/bluetooth.h"
#include "timer.h"
#include "init_mcu.h"

void init_sleep_mode(void)
{

	// Configure EM4 behavior
	EMU_EM4Init_TypeDef EM4_init = EMU_EM4INIT_DEFAULT;

	EM4_init.em4State = emuEM4Hibernate;  		// emuEM4Hibernate also works
	EM4_init.retainLfxo = false;						// Retain the 1KHz clock
	EM4_init.retainLfrco = false;
	EM4_init.retainUlfrco = true;
	EM4_init.pinRetentionMode = emuPinRetentionDisable;    // Do not retain pin-state

	EMU_EM4Init(&EM4_init);

	// enable wakeup from EM4
	CRYOTIMER_EM4WakeupEnable(true);

}

void go_to_sleep(void)
{
	start_timers();
	EMU_EnterEM1();
}

void go_to_deep_sleep(void)
{
	start_timers();
#ifdef DEBUG
	EMU_EnterEM2(0);
#else
	EMU_EnterEM3(1);
#endif
}
