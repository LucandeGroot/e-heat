/*
 * measurement.h
 *
 *  Created on: 23 aug. 2018
 *      Author: Koen_E-Trailer
 */

#ifndef E_LIB_TIMER_MEASUREMENT_H_
#define E_LIB_TIMER_MEASUREMENT_H_

#include <inttypes.h> // for uint

#define MEASUREMENT_TIMEOUT_US 	(2000)
#define SPEED_OF_SOUND_M_S 		(770.0) // meter per second
#define SPEED_OF_SOUND_CM_US 	(SPEED_OF_SOUND_M_S / 10000.0) // 10000 =  / 1000 / 1000 * 100   s -> ms -> us and m -> cm

/*
 *  /description  setup and start a new measurement
 */
void start_measurement(void);

/*
 *  /description Stop measurement and go to new state
 */
void stop_measurement(void);

/*
 *  /description Analyse the start and stop points
 */
double analyse_measurements(void);

/*
 *  /description measuring in progress
 *  /return 1 if measuring is busy
 *  /return 0 if measuring is done
 */
uint8_t measuring_in_progress(void);


#endif /* E_LIB_TIMER_MEASUREMENT_H_ */
