/*
 * timer.c
 *
 *  Created on: 18 dec. 2017
 *      Author: lucan
 */

#include "timer.h"
#include "em_timer.h"
#include "em_letimer.h"
#include "em_cryotimer.h"
#include "em_cmu.h" 	// for editing the clock
#include "em_emu.h"
#include "Decision_making.h"
#include "Communication/debug.h"

static uint8_t normal_timer_on = 0;
static uint8_t le_timer_on = 0;
static uint8_t cryo_timer_on = 0;

/*
 *  /description These functions are for normal timer
 */
// functions for the timer
static int8_t init_normal_timer(void);
void TIMER0_IRQHandler(void);
static int8_t start_timer(void);
static int8_t stop_timer(void);

/*
 *  /description These functions are for low energy timer
 */
// functions for the low energy timer
static int8_t init_le_timer();
void LETIMER0_IRQHandler();
static int8_t start_le_timer();
static int8_t stop_le_timer();

/*
 *  /description These functions are for Ultra low energy timer
 */
// functions for the cryotimer
static int8_t init_cryotimer();
static int8_t start_cryotimer();
static int8_t stop_cryotimer();
void CRYOTIMER_IRQHandler();



int8_t init_timer(uint8_t timer_type)
{
	switch(timer_type)
	{
		case TIMER_TYPE_NORMAL:
			normal_timer_on = 1;
			return init_normal_timer();
		break;

		case TIMER_TYPE_LOW_ENERGY:
			le_timer_on = 1;
			return init_le_timer();
		break;

		case TIMER_TYPE_ULTRA_LOW_ENERGY:
			cryo_timer_on = 1;
			return init_cryotimer();
		break;

		default:
			return -1; // no timer defined
		break;
	}
}

int8_t start_timers()
{
	if (normal_timer_on == 1)
	{
		start_timer();
	}
	if (le_timer_on == 1)
	{
		start_le_timer();
	}
	if (cryo_timer_on == 1)
	{
		start_cryotimer();
	}
	return 0;
}

int8_t stop_timers()
{

	stop_timer();

	stop_le_timer();

	stop_cryotimer();

	return 0;
}





int8_t init_normal_timer()
{
	// enable timer clock
	CMU_ClockEnable(cmuClock_TIMER0, true);
		// make sure this clock is one too
	CMU_ClockEnable(cmuClock_HFPER, true);
		/* Select TIMER0 parameters */
	TIMER_Init_TypeDef timerInit =
	{
		.enable = false, 						// do not start after init
		.debugRun = true,// run during debug
		.prescale = timerPrescale1024,// no prescale
		.clkSel = timerClkSelHFPerClk,// HFPCLK (38.4 Mhz)
		.fallAction = timerInputActionNone,// No action
		.riseAction = timerInputActionNone,// No action
		.mode = timerModeUp,// count up
		.dmaClrAct = false,// no DMA
		.quadModeX4 = false,// no quad mode x4/x2
		.oneShot = false,// no single shot timer.
		.sync = false,		// do not sync with other timers
	};
	TIMER_IntEnable(TIMER0, TIMER_IF_OF);    // Enable Timer0 overflow interrupt
	NVIC_EnableIRQ (TIMER0_IRQn);      // Enable TIMER0 interrupt vector in NVIC

	// set the correct top value
	// 38.4Mhz /
	TIMER_TopSet(TIMER0, 300000);

	// copy the config to the timer
	TIMER_Init(TIMER0, &timerInit);
	return 0;
}

int8_t start_timer()
{
	TIMER_Enable(TIMER0, 1);
	return 0;
}

int8_t stop_timer()
{
	TIMER_Enable(TIMER0, 0);
	return 0;
}

void TIMER0_IRQHandler()
{
	TIMER_IntClear(TIMER0, TIMER_IF_OF);
}


/*
 *  LOW ENERGY
 */
int8_t init_le_timer()
{
	CMU_ClockEnable(cmuClock_LETIMER0, 1);

	GPIO_PinModeSet(gpioPortC, 11, gpioModeWiredOrPullDown, 0);
	GPIO_SlewrateSet(gpioPortC, 0x7, 0x7); // max slew rate

	LETIMER_Init_TypeDef LETIMER_INIT = LETIMER_INIT_DEFAULT;

	LETIMER_INIT.debugRun = true;
	LETIMER_INIT.enable = false;

	LETIMER_Init(LETIMER0, &LETIMER_INIT);

	LETIMER0->CTRL |= LETIMER_CTRL_COMP0TOP;

	LETIMER_CompareSet(LETIMER0, 0, 7); // 30.5 us per tick

	LETIMER_IntEnable(LETIMER0, LETIMER_IEN_UF);
	NVIC_EnableIRQ(LETIMER0_IRQn);

	return 0;
}

int8_t start_le_timer()
{
	LETIMER_Enable(LETIMER0, true);
	return 0;
}

int8_t stop_le_timer()
{
	LETIMER_Enable(LETIMER0, false);
	return 0;
}

void LETIMER0_IRQHandler()
{	GPIO_PinOutSet(gpioPortC, 11);
	GPIO_PinOutClear(gpioPortC, 11);
	uint32_t flags = LETIMER_IntGet(LETIMER0);
	LETIMER_IntClear(LETIMER0, flags);
}

/*
 *  ULTRA LOW ENERGY
 */

int8_t init_cryotimer()
{
	CRYOTIMER_Init_TypeDef init = CRYOTIMER_INIT_DEFAULT;

	// Enable CRYOTIMER clock
	CMU_ClockEnable(cmuClock_CRYOTIMER, true);

	// Clear CRYOTIMER_IF PERIOD flag; it will be set upon EM4 wake
	CRYOTIMER_IntClear (CRYOTIMER_IF_PERIOD);

	/*
	* Set CRYOTIMER parameters.  Note that disabling the CRYOTIMER is
	* necessary after EM4 wake in order to reset the counter, otherwise
	* the next delay before wake won't be the full period.
	*/
	init.enable = false;				// Do not start after init
	init.em4Wakeup = true;				// Enable EM4 wakeup
	init.osc = cryotimerOscULFRCO;		// 1KHz
	init.presc = cryotimerPresc_1;		// no prescaler
	init.period = cryotimerPeriod_1k;  // period is 512 timer ticks (1/512Hz)

	CRYOTIMER_Init(&init);

	// Interrupt setup
	CRYOTIMER_IntClear(CRYOTIMER_IF_PERIOD);
	CRYOTIMER_IntEnable (CRYOTIMER_IEN_PERIOD);
	NVIC_ClearPendingIRQ (CRYOTIMER_IRQn);
	NVIC_EnableIRQ(CRYOTIMER_IRQn);

	return 0;
}

int8_t start_cryotimer()
{
	CRYOTIMER_Enable(true);
	DEBUG_SIMPLE("CRYO started", 1);
	return 0;
}

int8_t stop_cryotimer()
{
	CRYOTIMER_Enable(false);
	DEBUG_SIMPLE("CRYO stopped", 1);
	return 0;
}

void CRYOTIMER_IRQHandler()
{
	// Somethimes the uart crashes after coming out of EM3
#ifdef DEBUG
	init_uart();
#endif
	DEBUG_SIMPLE("CRYO INTERRUPT!", 1);

	//toggle_current_state();
	TIMER_IntClear(CRYOTIMER, TIMER_IF_OF);
	uint32_t flags = CRYOTIMER_IntGet();
	CRYOTIMER_IntClear(flags);

	// make sure the cryotimer is oneshot
	stop_cryotimer();
}
