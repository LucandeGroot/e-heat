/*
 * measurement.c
 *
 *  Created on: 23 aug. 2018
 *      Author: Koen_E-Trailer
 */


#include "measurement.h"
#include "GPIO/gpio.h"
#include "TDC1000/TDC1000.h"
#include "Timer/stopwatch.h"
#include "Decision_making.h"
#include "Communication/debug.h"
#include <stdio.h>
#include "gpio/externalclock.h"

#include "em_cmu.h"
#include "timer.h"

static uint8_t measurement_busy = 0;




void start_measurement(void)
{
	// we are measuring now
	measurement_busy = 1;

	// Wait for PSU to stabilize
	for(uint32_t i = 0; i < 0xFF; i++) __NOP();

	TDC1000_Enable_3V3();

	// Wait for PSU to stabilize
	for(uint32_t i = 0; i < 0xFFF; i++) __NOP();

	TDC1000_Enable_25V_Boost();

	// Disable all error flags so we know what's new
	TDC1000_DisableErrorFlags();

	// Enable the external clock for the TDC
	external_clock_enable();

	// reset measurements
	gpio_reset_measurements();

	// reset clock
	reset_stopwatch();

	// start timer
	start_stopwatch();

	// send trigger
	TDC1000_Trigger();

	// enable interrupts
	gpio_enable_irqs();

	// Disable the HV circuit again
	while(get_stopwatch_time() < TDC1000_TX_TIME*2);
	TDC1000_Disable_25V_Boost();
	TDC1000_Disable_3V3();
}

void stop_measurement(void)
{
	// disable interrupts
	gpio_disable_irqs();

	// stop measuring
	stop_stopwatch();

	// Reset all the potential error flags, saves some power to do it now
	TDC1000_DisableErrorFlags();

	// disable the external clock
	external_clock_disable();

	// we are done measuring
	measurement_busy = 0;
}

double convert_us_to_cm(uint64_t time){
	return((double)(SPEED_OF_SOUND_CM_US * (double)time));
}

double analyse_measurements(void)
{

#ifdef DEBUG
	char hexdata[50] = {0};
	sprintf(hexdata,"Start [%d]  Stop [%d]", timings.start_counts,timings.stop_counts );
	DEBUG_SIMPLE(hexdata,NEWLINE_TRUE);
	double distance;
	uint64_t measurement_ticks, delta_ticks;
#endif
	double delta_time;

	// get the START
	uint64_t start_ticks = convert_struct_to_ticks(timings.start_timings[0]);
	double start_time = convert_ticks_to_us(start_ticks);

	// Get the correct stop
	double stop_time;
	uint64_t stop_ticks;
	uint8_t i = 0;
	do {
		stop_ticks = convert_struct_to_ticks(timings.stop_timings[i]);
		stop_time = convert_ticks_to_us(stop_ticks);
		i++;
	} while (stop_time < start_time && i < MAX_STOPS);

	// Get the difference
	delta_time = stop_time - start_time;

	return delta_time;

#ifdef DEBUG
	for (uint16_t i = 0; i < timings.start_counts && i < MAX_STARTS ; i++)
	{
		// Extract the measurement ticks from the array
		measurement_ticks = convert_struct_to_ticks(timings.start_timings[i]);

		// substract the total time from the start time.
		delta_ticks = measurement_ticks - start_ticks;

		// convert to us
		delta_time = convert_ticks_to_us(delta_ticks);

		// convert to cm and halve it so we don't have the round trip distance
		distance = ((double)(SPEED_OF_SOUND_CM_US * (double)delta_time)/2.0);

		sprintf(hexdata,"Start [%d] = [%d] cm, delta [%d] us", (uint32_t)delta_ticks, (uint16_t)distance, (uint16_t)(delta_time/i));
		DEBUG_SIMPLE(hexdata, NEWLINE_TRUE);
	}
	for (uint16_t i = 0; i < timings.stop_counts && i < MAX_STOPS; i++)
	{
		// Extract the measurement ticks from the array
		measurement_ticks = convert_struct_to_ticks(timings.stop_timings[i]);

		// substract the total time from the start time.
		delta_ticks = measurement_ticks - start_ticks;

		// convert to us
		delta_time = convert_ticks_to_us(delta_ticks);

		// convert to cm
		distance = ((double)(SPEED_OF_SOUND_CM_US * (double)delta_time)/2.0);

		sprintf(hexdata,"Stop [%d] = [%d] cm, delta [%d] us", (uint32_t)delta_ticks, (uint16_t)distance, (uint16_t)(delta_time/(i+1)));
		DEBUG_SIMPLE(hexdata, NEWLINE_TRUE);
	}
#endif

	// Get the distance
	//return convert_us_to_cm(delta_time)/2.0;
}


uint8_t measuring_in_progress(void)
{
	return measurement_busy;
}
