/*
 * Stopwatch.c
 *
 *  Created on: 23 aug. 2018
 *      Author: Koen_E-Trailer
 */

#ifndef E_LIB_TIMER_STOPWATCH_C_
#define E_LIB_TIMER_STOPWATCH_C_

#include <stdint.h>
#include "GPIO/gpio.h"

#define CLOCK_DEVIDER	1       // sync this with the prescaler

float CLOCK_SPEED_MHz;
uint32_t CLOCK_SPEED_Hz;


// Parameter to keep track of the stopwatch overflows
extern volatile uint32_t STOPWATCH_OVERFLOWS;

/*
 * 	/description init the stopwatch
 * 	/return -1 if error
 * 	/return 0 if no error
 */
int8_t init_stopwatch(void);

/*
 * 	/description start the stopwatch
 * 	/return -1 if error
 * 	/return 0 if no error
 */
int8_t start_stopwatch(void);

/*
 * 	/description stop the stopwatch
 * 	/return -1 if error
 * 	/return 0 if no error
 */
int8_t stop_stopwatch(void);

/*
 * 	/description reset the stopwatch
 * 	/return -1 if error
 * 	/return 0 if no error
 */
int8_t reset_stopwatch(void);

/*
 * 	/description get the stopwatch ticks
 * 	/return -1 if error
 * 	/return 0 if no error
 */
uint16_t get_stopwatch_ticks(void);

/*
 * 	/description get the stopwatch overflows
 * 	/return The amount of stopwatch overflows
 */
uint16_t get_stopwatch_overflows(void);

/*
 * 	/description get the stopwatch time in us
 * 	/return -1 if error
 * 	/return 0 if no error
 */
uint64_t get_stopwatch_time(void);


/*
 * 	/description Convert the ticks to us
 * 	/return ticks / Fcpu
 */
float convert_ticks_to_us(uint64_t ticks);

/*
 * 	/description Convert the struct to ticks
 * 	/return the overlfows + timer count
 */
uint64_t convert_struct_to_ticks(GPIO_measurement_t measurement);

/*
 *
 * 	/description Convert the struct to microseconds
 * 	/return the amount of microseconds in the struct
 */
uint16_t convert_struct_to_us(GPIO_measurement_t measurement);


/*
 *	/description Wait for a certain amount of time, this function is blocking
 */
void wait_for_stopwatch(uint64_t time_to_wait);

#endif /* E_LIB_TIMER_STOPWATCH_C_ */
