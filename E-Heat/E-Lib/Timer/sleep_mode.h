/*
 * sleep_mode.h
 *
 *  Created on: 3 sep. 2018
 *      Author: Koen_E-Trailer
 */

#ifndef LIB_TIMER_SLEEP_MODE_H_
#define LIB_TIMER_SLEEP_MODE_H_




/*
 *	/description Initialize the sleepmode
 */
void init_sleep_mode(void);

/*
 *	/description go_to_sleep
 */
void go_to_sleep(void);

/*
 *	/description go_to_deep_sleep
 */
void go_to_deep_sleep(void);

#endif /* LIB_TIMER_SLEEP_MODE_H_ */
