/*
 * timer.h
 *
 *  Created on: 20-8-2018
 *      Author: Koen
 */

#ifndef TIMER_H_
#define TIMER_H_

#include <stdint.h>

#define TIMER_TYPE_NORMAL 			0
#define TIMER_TYPE_LOW_ENERGY 		1
#define TIMER_TYPE_ULTRA_LOW_ENERGY 2
/*
 *	/description This is the main timer function. In here we say what timer we init
 *	/Return -1 if error
 *	/Return 0 if error
 *	/input Timer_type -> see defines
 */
int8_t init_timer(uint8_t timer_type);

/*
 * 	/description This will start the timer.
 *	/Return -1 if error
 *	/Return 0 if error
 */
int8_t start_timers();

/*
 * 	/description This will stop the timer.
 *	/Return -1 if error
 *	/Return 0 if error
 */
int8_t stop_timers();














#endif /* TIMER_H_ */
