/*
 * Stopwatch.c
 *
 *  Created on: 23 aug. 2018
 *      Author: Koen_E-Trailer
 */

#include "stopwatch.h"

#include <em_timer.h>
#include <em_cmu.h>
#include "Communication/debug.h"

float CLOCK_SPEED_MHz = 0;
uint32_t CLOCK_SPEED_Hz_ = 0;
TIMER_TypeDef* STOPWATCH_TIMER = TIMER1;
volatile uint32_t STOPWATCH_OVERFLOWS = 0;

#define US_PER_OVERFLOW (65535.0 / (CLOCK_SPEED_MHz * CLOCK_DEVIDER)) //max overflow tick / clockspeed

void TIMER1_IRQHandler()
{
	STOPWATCH_OVERFLOWS++;
	TIMER_IntClear(STOPWATCH_TIMER, TIMER_IF_OF);
}

int8_t init_stopwatch(void)
{
	// TODO: convert this to capture mode (TIMER_InitCC)

	// enable timer clock
	CMU_ClockEnable(cmuClock_TIMER1, true);
		// make sure this clock is one too
	CMU_ClockEnable(cmuClock_HFPER, true);
		/* Select TIMER0 parameters */
	TIMER_Init_TypeDef timerInit =
	{
		.enable = false, 						// do not start after init
		.debugRun = true,						// run during debug
		.prescale = timerPrescale1,				// /8 prescaler
		.clkSel = timerClkSelHFPerClk,			// HFPCLK (38.4 Mhz)
		.fallAction = timerInputActionNone,		// No action
		.riseAction = timerInputActionNone,		// No action
		.mode = timerModeUp,					// count up
		.dmaClrAct = false,						// no DMA
		.quadModeX4 = false,					// no quad mode x4/x2
		.oneShot = false,						// no single shot timer.
		.sync = false,							// do not sync with other timers
	};
	TIMER_IntEnable(STOPWATCH_TIMER, TIMER_IF_OF);    // Enable Timer0 overflow interrupt
	NVIC_EnableIRQ (TIMER1_IRQn);      // Enable TIMER0 interrupt vector in NVIC

	// set the correct top value
	// 38.4Mhz /
//	TIMER_TopSet(STOPWATCH_TIMER, 200000);

	// Get the clockspeed for the timer
	CLOCK_SPEED_Hz = CMU_ClockFreqGet(cmuClock_TIMER1);
	CLOCK_SPEED_MHz = (float)CLOCK_SPEED_Hz / (float)1000000.0;


	// copy the config to the timer
	TIMER_Init(STOPWATCH_TIMER, &timerInit);
	return 0;
}

int8_t start_stopwatch(void)
{
	TIMER_Enable(STOPWATCH_TIMER, true);
	return 0;
}

int8_t stop_stopwatch(void)
{
	TIMER_Enable(STOPWATCH_TIMER, false);
	return 0;
}

int8_t reset_stopwatch(void)
{
	STOPWATCH_OVERFLOWS = 0;
	TIMER_CounterSet(STOPWATCH_TIMER,0);
	return 0;
}

__INLINE uint16_t get_stopwatch_ticks(void)
{
	return TIMER_CounterGet(STOPWATCH_TIMER);
}

__INLINE uint16_t get_stopwatch_overflows(void) {
	return STOPWATCH_OVERFLOWS;
}

float convert_ticks_to_us(uint64_t ticks)
{
	float time = (uint32_t)((uint32_t)ticks / (uint32_t)(CLOCK_SPEED_MHz * CLOCK_DEVIDER));

	return time;
}

uint64_t convert_struct_to_ticks(GPIO_measurement_t measurement) {
	return (uint64_t)measurement.timer_count
			+ (uint64_t)((uint64_t)0xFFFF * (uint64_t)measurement.timer_overflows);
}

uint64_t get_stopwatch_time(void)
{
	uint64_t time_in_overflows = (uint64_t)STOPWATCH_OVERFLOWS * (uint64_t)US_PER_OVERFLOW;
	uint64_t time_in_counter = (uint64_t)get_stopwatch_ticks() / (uint64_t)(CLOCK_SPEED_MHz * CLOCK_DEVIDER);

	// Time in us
	//uint64_t time = (((uint64_t)STOPWATCH_OVERFLOWS * (uint64_t)US_PER_OVERFLOW) + (uint64_t)get_stopwatch_ticks()) / (CLOCK_SPEED_MHz * CLOCK_DEVIDER);

	return time_in_overflows + time_in_counter;
}

uint16_t convert_struct_to_us(GPIO_measurement_t measurement) {
	return convert_ticks_to_us(convert_struct_to_ticks(measurement));
}


void wait_for_stopwatch(uint64_t time_to_wait) {
	uint64_t time = 0;
	do {
		time = get_stopwatch_time();
	} while (time < time_to_wait);
}
