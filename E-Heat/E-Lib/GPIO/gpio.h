/*
 * gpio.h
 *
 *  Created on: 27 feb. 2018
 *      Author: Koen_E-Trailer
 */

#ifndef LIB_GPIO_H_
#define LIB_GPIO_H_

#include <stdint.h>
#include "em_gpio.h"
#include "debug.h"
#include "TDC1000_defs.h"



#define PIN_START_PORT 	gpioPortC
#define PIN_START_NUM	11

#define PIN_STOP_PORT	gpioPortD
#define PIN_STOP_NUM	14


#define INTERUPT_RISING_EDGE	true
#define INTERUPT_FALLING_EDGE	true

#define INTERUPT_ENABLED		true
#define INTERUPT_DISABLED		false


#ifdef DATA_AGGREGATION
#define MAX_STOPS    	TDC1000_MAX_STOP*2
#define MAX_STARTS    	5
#else
#define MAX_STOPS    	4*2
#define MAX_STARTS    	1
#endif

typedef struct {
	uint16_t timer_count;
	uint16_t timer_overflows;
} GPIO_measurement_t;

typedef struct  {
	GPIO_measurement_t stop_timings[MAX_STOPS];
	GPIO_measurement_t start_timings[MAX_STARTS];
	uint8_t stop_counts;
	uint8_t start_counts; // should be 1
} GPIO_timings_t;

extern volatile GPIO_timings_t timings;

int8_t gpio_init(void);
void gpio_enable_irqs(void);
void gpio_disable_irqs(void);
void gpio_reset_measurements(void);
GPIO_timings_t * ret_adres(void);








#endif /* LIB_GPIO_H_ */
