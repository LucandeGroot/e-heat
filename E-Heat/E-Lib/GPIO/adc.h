

 /* adc.h
 *
 *  Created on: 2 nov. 2017
 *      Author: lucan
 */

#ifndef ADC_H_
#define ADC_H_

#include <stdint.h> // for uint

#define TEMP_SLOPE_BGM113	(-1.84) // mV/C. from table 4.22 in the BGM113 datasheet




// functions
void initADC(uint32_t posSel);
void initADC_scan(uint32_t posSel);

uint32_t readADC0();
uint32_t readADC0_scan();
void readADC0_string();

float adc_get_temp(void);

#endif // ADC_H_
