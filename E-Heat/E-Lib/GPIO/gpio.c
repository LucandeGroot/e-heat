/*
 * gpio.c
 *
 *  Created on: 27 feb. 2018
 *      Author: Koen_E-Trailer
 */

#include "gpio.h"
#include "stopwatch.h"
#include <string.h> // for memset
#include "debug.h"

void GPIO_ODD_IRQHandler(void);
void GPIO_EVEN_IRQHandler(void);

volatile GPIO_timings_t timings;

void GPIO_EVEN_IRQHandler(void)
{
	// current time
	timings.stop_timings[timings.stop_counts].timer_count = get_stopwatch_ticks();
	timings.stop_timings[timings.stop_counts].timer_overflows = get_stopwatch_overflows();
	timings.stop_counts++;

	// Make sure we don't overfill the array
	if (timings.stop_counts >= MAX_STOPS)
	{
		GPIO_IntDisable((1<<PIN_STOP_NUM));
	}

	// Clear the intterupt flags
	uint32_t flags = GPIO_IntGet();
	GPIO_IntClear(flags);
}

void GPIO_ODD_IRQHandler(void)
{
	// current time
	timings.start_timings[timings.start_counts].timer_count = get_stopwatch_ticks();
	timings.start_timings[timings.start_counts].timer_overflows = get_stopwatch_overflows();
	timings.start_counts++;

	// Make sure we dont overfill the array
	if (timings.start_counts >= MAX_STARTS)
	{
		//timings.start_counts = 0; // OVERFLOW!
		GPIO_IntDisable((1<<PIN_START_NUM));
	}

	// Clear the interrupt flags
	uint32_t flags = GPIO_IntGet();
	GPIO_IntClear(flags);
}


int8_t gpio_init(void)
{
	GPIO_PinModeSet(PIN_START_PORT, PIN_START_NUM, gpioModeInputPull, 0);    // <<-- INPUT
	GPIO_PinModeSet(PIN_STOP_PORT, PIN_STOP_NUM, gpioModeInputPull, 0);    // <<-- INPUT

	GPIO_ExtIntConfig(	PIN_START_PORT,
						PIN_START_NUM,
						PIN_START_NUM,
						INTERUPT_RISING_EDGE,
						false,
						INTERUPT_ENABLED);

	GPIO_ExtIntConfig(	PIN_STOP_PORT,
						PIN_STOP_NUM,
						PIN_STOP_NUM,
						INTERUPT_RISING_EDGE,
						INTERUPT_FALLING_EDGE,
						INTERUPT_ENABLED);

	gpio_reset_measurements();
	return 0;
}

inline void gpio_enable_irqs()
{
	GPIO_IntEnable((1<<PIN_STOP_NUM) | (1<<PIN_START_NUM));

	NVIC_EnableIRQ(GPIO_ODD_IRQn);
	NVIC_EnableIRQ(GPIO_EVEN_IRQn);
}

inline void gpio_disable_irqs()
{
	NVIC_DisableIRQ(GPIO_ODD_IRQn);
	NVIC_DisableIRQ(GPIO_EVEN_IRQn);

	GPIO_IntDisable((1<<PIN_STOP_NUM) | (1<<PIN_START_NUM));
}

void gpio_reset_measurements()
{
	memset(&timings, 0, sizeof(timings));
}

GPIO_timings_t * ret_adres(void)
{
	return &timings;
}

