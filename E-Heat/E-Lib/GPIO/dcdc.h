/*
 * dcdc.h
 *
 *  Created on: 16 jan. 2018
 *      Author: lucan
 */

#ifndef DCDC_H_
#define DCDC_H_

#if defined(HAL_CONFIG)
#include "bsphalconfig.h"
#include "hal-config.h"
#else
#include "bspconfig.h"
#endif

#include "board_features.h"

#include "em_chip.h"
#include "em_cmu.h"
#include "em_emu.h"
#include "em_rtcc.h"

#include "bsp.h"

#include "tempdrv.h"

void init_dcdc();
void disable_radio();
void enable_radio();

void initMcu_powersaving();

int8_t get_supply_stable(void);

int8_t wait_till_stable();

#endif /* DCDC_H_ */
