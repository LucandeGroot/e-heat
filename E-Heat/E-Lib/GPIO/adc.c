/* adc.c
 *
 *  Created on: 2 nov. 2017
 *      Author: lucan
 */
#include <em_adc.h>
#include <stdlib.h> // for itoa
#include <stdio.h> // for memcpy
#include <string.h>
#include <inttypes.h> // for sprintf with uint32_t
#include "adc.h"
#include "native_gecko.h" // for the softwaretimer
#include "em_emu.h"
#include "em_cmu.h"
#include "em_gpio.h"
#include "Communication/bluetooth.h" 	// for the message array
#include "Communication/uart.h"		// for DEBUG_MESSAGE
#include "Communication/debug.h"

void initADC(uint32_t posSel)
{
// reset adc0
	//ADC_Reset(ADC0);

	//Enable ADC clock
	CMU_ClockEnable(cmuClock_ADC0, true);

	//Base the ADC configuration on the default setup.
	ADC_Init_TypeDef init = ADC_INIT_DEFAULT;
	ADC_InitSingle_TypeDef sInit = ADC_INITSINGLE_DEFAULT;

	//Initialize timebases
	init.timebase = ADC_TimebaseCalc(0);		// use default reference clock
	init.prescale = ADC_PrescaleCalc(32000, 0);		// 32KHz
	init.ovsRateSel = adcOvsRateSel2;//adcOvsRateSel4096;//adcOvsRateSel2;				// 2x oversampling (unused)
	init.warmUpMode = adcWarmupKeepInSlowAcq;//adcWarmupKeepADCWarm;//adcWarmupKeepInSlowAcq;	// used for high impedence, low freqency readings.
	init.em2ClockConfig = adcEm2Disabled;		// ADC clock is on-demand on in EM2 DOES NOT WORK
	ADC_Init(ADC0, &init);

	//Set input to temperature sensor. Reference must be 1.25V
	if ( posSel == adcPosSelAPORT3XCH6 || posSel == adcPosSelAPORT4XCH5 || posSel == adcPosSelAPORT4XCH7 || posSel == adcPosSelAPORT3YCH7)
	{
		sInit.reference = adcRefVDD; // 3.3V
		//sInit.reference = adcRef2V5;
	} else {
		sInit.reference = adcRef1V25;
	}


	sInit.acqTime = adcAcqTime256;//adcAcqTime128;//adcAcqTime256;			// highest aquasistion time (256 clock cycles)
	sInit.posSel = posSel;    	// PD13
	sInit.resolution = adcRes12Bit; 		// 12 Bit
	sInit.leftAdjust = false;				// Do not left adjust
	ADC_InitSingle(ADC0, &sInit);


	// dummyread the adc once
	readADC0();
	ADC_Start(ADC0, adcStartSingle);
}

void initADC_scan(uint32_t posSel)
{
	// reset adc0
		ADC_Reset(ADC0);

		//Enable ADC clock
		CMU_ClockEnable(cmuClock_ADC0, true);

		//Base the ADC configuration on the default setup.
		ADC_Init_TypeDef init = ADC_INIT_DEFAULT;
		ADC_InitScan_TypeDef sInit = ADC_INITSCAN_DEFAULT;

		//Initialize timebases
		init.timebase = ADC_TimebaseCalc(0);		// use default reference clock
		init.prescale = ADC_PrescaleCalc(64000, 0);		// 32KHz
		init.ovsRateSel = adcOvsRateSel4096;//adcOvsRateSel4096;//adcOvsRateSel2;				// 2x oversampling (unused)
		init.warmUpMode = adcWarmupKeepADCWarm;//adcWarmupNormal;//adcWarmupKeepInSlowAcq;	// used for high impedence, low freqency readings.
		init.em2ClockConfig = adcEm2Disabled;		// ADC clock is on-demand on in EM2 DOES NOT WORK
		ADC_Init(ADC0, &init);

		//Set input to temperature sensor. Reference must be 1.25V
		/*if (posSel == adcPosSelTEMP || posSel == adcPosSelAPORT3XCH6 || posSel == adcPosSelAPORT4XCH5 || posSel == adcPosSelAPORT4XCH7 || posSel == adcPosSelAPORT3YCH7)
		{*/
			sInit.reference = adcRef2V5; // 3.3V
		/*} else {
			sInit.reference = adcRef1V25;
		}*/

		sInit.acqTime = adcAcqTime256;//adcAcqTime256;			// highest aquasistion time (256 clock cycles)
		//sInit.prsSel = adcPRSSELCh3;    	// PD13
		sInit.resolution = adcRes12Bit; 		// 12 Bit
		sInit.leftAdjust = false;				// Do not left adjust
		sInit.rep		= true;
		sInit.fifoOverwrite = true;
		ADC_ScanSingleEndedInputAdd(&sInit,adcScanInputGroup2,posSel);
		ADC_InitScan(ADC0, &sInit);

		// dummyread the adc once
		//readADC0();
		ADC_Start(ADC0, adcStartScan);
}

uint32_t readADC0_scan()
{

	/*char hexdata[50] = {0};
	uint32_t counter = 0;
	uint32_t before = ADC0->STATUS;
	uint32_t tussen = 0;
	uint32_t after = 0;*/

	//Wait while conversion is active
	while ((ADC0->STATUS & ADC_STATUS_SCANDV) != ADC_STATUS_SCANDV && ADC0->SINGLEFIFOCOUNT == 4){
		//counter++;
	}
	//tussen = ADC0->STATUS;

	//Get ADC result
	uint32_t adc_Result = ADC_DataScanGet(ADC0);
	uint32_t adc_Result1 = ADC_DataScanGet(ADC0);
	uint32_t adc_Result2 = ADC_DataScanGet(ADC0);
	uint32_t adc_Result3 = ADC_DataScanGet(ADC0);

	adc_Result = adc_Result/4 + adc_Result1/4 + adc_Result2/4 + adc_Result3/4;
	/*after = ADC0->STATUS;
	sprintf(hexdata,"\ncounter %d, %d, %d, %d, %d, %d",counter, before, tussen, after, adc_Result, ADC0->SCANFIFOCOUNT);
	DEBUG_MESSAGE((uint8_t*) hexdata, sizeof(hexdata), NEWLINE_FALSE);
*/


	//CMU_ClockEnable(cmuClock_ADC0, false);
	return adc_Result;
}

uint32_t readADC0() {
	// start the conversion
	ADC_Start(ADC0, adcStartSingle); //start new measurement

	//Wait while conversion is active
	while ((ADC0->STATUS & ADC_STATUS_SINGLEDV) != ADC_STATUS_SINGLEDV){
		;
	}

	//Get ADC result
	uint32_t adc_Result = ADC_DataSingleGet(ADC0);

	return adc_Result;
}

static float ConvertToCelsius(int32_t adcSample)
{
  uint32_t calTemp0;
  uint32_t calValue0;
  int32_t readDiff;
  float temp;

  /* Factory calibration temperature from device information page. */
  calTemp0 = ((DEVINFO->CAL & _DEVINFO_CAL_TEMP_MASK)
              >> _DEVINFO_CAL_TEMP_SHIFT);

  calValue0 = ((DEVINFO->ADC0CAL3
                /* _DEVINFO_ADC0CAL3_TEMPREAD1V25_MASK is not correct in
                    current CMSIS. This is a 12-bit value, not 16-bit. */
                & 0xFFF0)
               >> _DEVINFO_ADC0CAL3_TEMPREAD1V25_SHIFT);

  if ((calTemp0 == 0xFF) || (calValue0 == 0xFFF))
  {
    /* The temperature sensor is not calibrated */
    return -100.0;
  }

  /* Vref = 1250mV
     TGRAD_ADCTH = 1.84 mV/degC (from datasheet)
  */
  readDiff = calValue0 - adcSample;
  temp     = ((float)readDiff * 1250);
  temp    /= (4096 * TEMP_SLOPE_BGM113);

  /* Calculate offset from calibration temperature */
  temp     = (float)calTemp0 - temp;
  return temp;
}

float adc_get_temp(void)
{

	initADC(adcPosSelTEMP);
	uint32_t adc = readADC0();

	// Reset the adc again
	ADC_Reset(ADC0);

	// reset the other adc registers
	ADC0->CMPTHR = _ADC_CMPTHR_RESETVALUE;
	ADC0->CMD = _ADC_CMD_RESETVALUE;

	CMU_ClockEnable(cmuClock_ADC0, false);

	// Convert to temperature
	return ConvertToCelsius(adc);
}
