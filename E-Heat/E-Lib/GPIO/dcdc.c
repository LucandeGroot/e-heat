/*
 * dcdc.c
 *
 *  Created on: 16 jan. 2018
 *      Author: lucan
 */

#include "dcdc.h"
#include "em_emu.h"

EMU_VmonInit_TypeDef Voltage_monitor = EMU_VMONINIT_DEFAULT;

void init_dcdc() {

	EMU_DCDCInit_TypeDef dcdcInit = EMU_DCDCINIT_DEFAULT;
	dcdcInit.em01LoadCurrent_mA = 7;
	dcdcInit.em234LoadCurrent_uA = 700;
	dcdcInit.dcdcMode = emuDcdcMode_LowNoise;

	// Best power in EM3
	dcdcInit.mVout = 3300;
	dcdcInit.anaPeripheralPower = emuDcdcAnaPeripheralPower_AVDD;
	dcdcInit.dcdcLnCompCtrl = emuDcdcLnCompCtrl_4u7F;

	EMU_DCDCInit(&dcdcInit);

	disable_radio();

	Voltage_monitor.threshold = 3300;

	EMU_VmonInit(&Voltage_monitor);

	EMU_VmonChannelStatusGet(Voltage_monitor.channel);
}

void disable_radio() {
	// use this when the radio is disabled
	//EMU_DCDCConductionModeSet(emuDcdcConductionMode_DiscontinuousLN, true);
}

void enable_radio() {
	// use this when the radio is enabled
	//EMU_DCDCConductionModeSet(emuDcdcConductionMode_ContinuousLN, true);
}


void initMcu_powersaving()
{
	  // Automatically start and select HFXO
	  //CMU_HFXOAutostartEnable(0, false, false);

	  // Enable LFXO oscillator, and wait for it to be stable
	/*  CMU_OscillatorEnable(cmuOsc_ULFRCO, true, true);

	  CMU_ClockSelectSet(cmuClock_LFA,cmuSelect_ULFRCO);
	  CMU_ClockSelectSet(cmuClock_LFB,cmuSelect_ULFRCO);
	  CMU_ClockSelectSet(cmuClock_LFE,cmuSelect_ULFRCO);

	  // HFRCO not needed when using HFXO
	  //CMU_OscillatorEnable(cmuOsc_HFXO, false, false);
	  CMU_OscillatorEnable(cmuOsc_LFXO, false, false);*/

		CMU_ClockEnable(cmuClock_TIMER0,false);
		CMU_ClockEnable(cmuClock_TIMER1,false);

#ifndef DEBUG
		CMU_ClockEnable(cmuClock_USART0,false);
		CMU_ClockEnable(cmuClock_USART1,false);
#endif
		CMU_ClockEnable(cmuClock_HF,false);
		CMU_ClockEnable(cmuClock_DBG,false);
		CMU_ClockEnable(cmuClock_AUX,false);
		CMU_ClockEnable(cmuClock_EXPORT,false);
		CMU_ClockEnable(cmuClock_BUS,false);
		CMU_ClockEnable(cmuClock_CRYPTO,false);
		CMU_ClockEnable(cmuClock_LDMA,false);
		CMU_ClockEnable(cmuClock_GPCRC,false);
		//CMU_ClockEnable(cmuClock_HFLE,false);
		//CMU_ClockEnable(cmuClock_PRS,false);
		CMU_ClockEnable(cmuClock_ACMP0,false);//
		CMU_ClockEnable(cmuClock_ACMP1,false);
		CMU_ClockEnable(cmuClock_IDAC0,false);
		CMU_ClockEnable(cmuClock_ADC0,false);
		CMU_ClockEnable(cmuClock_I2C0,false);
		CMU_ClockEnable(cmuClock_LFA,false);//

#ifndef DEBUG
		CMU_ClockEnable(cmuClock_LETIMER0,false);
#endif
		CMU_ClockEnable(cmuClock_PCNT0,false);

		CMU_ClockEnable(cmuClock_LFB,false);
		CMU_ClockEnable(cmuClock_LEUART0,false);
		CMU_ClockEnable(cmuClock_LFE,false);
		//CMU_ClockEnable(cmuClock_RTCC,false);
		CMU_ClockEnable(cmuClock_ADC0ASYNC,false);
}

int8_t get_supply_stable()
{
	int8_t ret = EMU_VmonChannelStatusGet(Voltage_monitor.channel);
	return ret;
}

int8_t wait_till_stable()
{
	int timeout = 10000;
	while(timeout >= 0)
	{
		if (get_supply_stable() == 1)
		{

			return 0;
		}
		timeout--;
	}

			#ifdef DEBUG
				char hexdata[10] = {0};
				sprintf(hexdata, " %d", 10000 - timeout);
				DEBUG_SIMPLE((uint8_t*)" timeout", 0);
			#endif

	return -1;
}

