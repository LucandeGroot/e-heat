/*
 * Settings_config.h
 *
 *  Created on: 25 sep. 2018
 *      Author: lucan
 */

#ifndef LIB_SETTINGS_CONFIG_H_
#define LIB_SETTINGS_CONFIG_H_

// for how long we send dummy data at startup
#define SETTINGS_INIT_SECONDS 	0

#endif /* LIB_SETTINGS_CONFIG_H_ */
