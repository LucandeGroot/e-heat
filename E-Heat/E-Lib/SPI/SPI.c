/*
 * SPI.c
 *
 *  Created on: 20 aug. 2018
 *      Author: lucan
 */

#include "SPI.h"
#include "em_usart.h"
#include "em_cmu.h"

static USART_TypeDef * SPI_REG = USART0;

void spi_write (uint8_t address, uint8_t c) {
	address |= 0b01000000; // add the write byte

	USART_Tx(SPI_REG, address);
	USART_Tx(SPI_REG, c);
}

uint8_t spi_read (uint8_t address) {
	USART_Tx(SPI_REG, address);	// read command + address

	//TODO: find out which register is the correct one to read
	uint8_t temp = USART_SpiTransfer(SPI_REG, 0x00); // read while sending dummy bytes to keep the clock going
	temp = (uint8_t)SPI_REG->RXDATA;
	temp = (uint8_t)SPI_REG->RXDATAX;
	temp = (uint8_t)SPI_REG->RXDATAXP;
	temp = (uint8_t)SPI_REG->RXDOUBLE;
	temp = (uint8_t)SPI_REG->RXDOUBLEX;
	temp = (uint8_t)SPI_REG->RXDOUBLEXP;

	return temp;
}

void spi_reset() {
	USART_Reset(SPI_REG);

	GPIO_PinModeSet(SPI_CS_PORT, SPI_CS_PIN, gpioModeDisabled, 0);
	GPIO_PinModeSet(SPI_SCK_PORT, SPI_SCK_PIN, gpioModeDisabled, 0);
	GPIO_PinModeSet(SPI_MISO_PORT, SPI_MISO_PIN, gpioModeDisabled, 0);
	GPIO_PinModeSet(SPI_MOSI_PORT, SPI_MOSI_PIN, gpioModeDisabled, 0);

	CMU_ClockEnable(cmuClock_USART0, false);
}

void spi_init( uint8_t master, uint32_t baud) {
	USART_InitSync_TypeDef SPI_init = USART_INITSYNC_DEFAULT;

	// Enable the clocks
	CMU_ClockEnable(cmuClock_HFPER, true);		// High Performance Peripheral clock
	CMU_ClockEnable(cmuClock_USART0, true);		// Enable USART0 clock
	CMU_ClockEnable(cmuClock_GPIO, true);		// Enable GPIO clock

	// configure the IO pins
	GPIO_PinModeSet(SPI_CS_PORT, SPI_CS_PIN, gpioModePushPull, 0);
	GPIO_PinModeSet(SPI_SCK_PORT, SPI_SCK_PIN, gpioModePushPull, 1);
	GPIO_PinModeSet(SPI_MISO_PORT, SPI_MISO_PIN, gpioModeInputPull, 0);
	GPIO_PinModeSet(SPI_MOSI_PORT, SPI_MOSI_PIN, gpioModePushPull, 0);

	SPI_init.autoCsEnable = true;			// Auto slave select
	SPI_init.autoCsHold = 0;				// Do not select the CS one clock after TX
	SPI_init.autoCsSetup = 0;				// Do not select the CS one clock before TX
	SPI_init.autoTx = false;				// Do not automatically transmit message once it is in the buffer
	SPI_init.baudrate = baud;				// Speed of the SPI clock, translates to bits per second
	SPI_init.clockMode = usartClockMode3; 	// Based on Figure 40. SPI Protocol in the TDC1000 datasheet
	SPI_init.databits = usartDatabits8;		// 8 bit messages
	SPI_init.enable = usartDisable;			// Do not start the SPI after init
	SPI_init.master = master;				// Whether to set to master mode
	SPI_init.msbf = true;					// Send msb first
	SPI_init.prsRxEnable = false;			// Do not use the Peripheral Reflex System
	SPI_init.refFreq = 0;					// use current clock for speed calculation


	// Apply the settings
	USART_InitSync(SPI_REG, &SPI_init);

	/* Connect the correct pins to the register
	* See the ERF32BG1 datasheet Table 6.5. Alternate Functionality Overview
	* CS = PF2 = LOC23
	* CLK = PB13 = LOC6
	* MISO = PC11 = LOC15
	* MOSI = PB12 = LOC7
	*/
	SPI_REG->ROUTELOC0 = 	USART_ROUTELOC0_CLKLOC_LOC6 | 	// Clock
							USART_ROUTELOC0_TXLOC_LOC7 |	// MOSI
							USART_ROUTELOC0_RXLOC_LOC15 |	// MISO
							USART_ROUTELOC0_CSLOC_LOC23;	// Slave Select

	// Enable Clock, TX, RX and Slave select
	SPI_REG->ROUTEPEN = USART_ROUTEPEN_CLKPEN | USART_ROUTEPEN_TXPEN | USART_ROUTEPEN_RXPEN | USART_ROUTEPEN_CSPEN;

	USART_Enable(SPI_REG, usartEnable);
}

