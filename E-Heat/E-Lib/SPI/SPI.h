/*
 * SPI.h
 *
 *  Created on: 20 aug. 2018
 *      Author: lucan
 */

#ifndef SPI_SPI_H_
#define SPI_SPI_H_

#include <inttypes.h>
#include "em_gpio.h"

// IO pin definitions
#define SPI_CS_PORT 	gpioPortF
#define SPI_CS_PIN		2

#define SPI_SCK_PORT	gpioPortB
#define SPI_SCK_PIN  	13

#define SPI_MISO_PORT 	gpioPortB
#define SPI_MISO_PIN	11

#define SPI_MOSI_PORT	gpioPortB
#define SPI_MOSI_PIN	12

//#define SPI_RX_BUFFER_SIZE 128 // bytes

// Receive buffer
// Make it a circular buffer later
//volatile uint8_t SPI_RX_BUFFER[SPI_RX_BUFFER_SIZE];
//volatile uint8_t SPI_RX_BUFFER_INDEX;

void spi_write (uint8_t address, uint8_t c);
uint8_t spi_read (uint8_t address);
void spi_init (uint8_t master, uint32_t baud);
void spi_reset();


#endif /* SPI_SPI_H_ */
