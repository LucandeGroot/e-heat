/*
 * Decision_making.c
 *
 *  Created on: 21 aug. 2018
 *      Author: Koen_E-Trailer
 */

#include <stopwatch.h>
#include "Decision_making.h"
#include "Communication/bluetooth.h"
#include "GPIO/gpio.h"
#include "Timer/timer.h"
#include "Timer/measurement.h"
#include "Communication/debug.h"
#include "TDC1000.h"
#include <stdio.h>
#include "sleep_mode.h"
#include "GPIO/externalclock.h"
#include "GPIO/adc.h"
#include "em_emu.h"

E_GAS_STATES_t E_GAS_STATES;

uint16_t values_to_send[MAX_READINGS_IN_MESSAGE] = {0};

int8_t Decision_making_init()
{
	// Initialize the START and STOP interrupts and pins
	gpio_init();

	// Init timers for controlling the state timeout
	init_timer(TIMER_TYPE_ULTRA_LOW_ENERGY);
	init_stopwatch();

	external_clock_init();

	//Make a sample measurement to initialize everything
	start_measurement();
	stop_measurement();

	E_GAS_STATES = ST_START_MEASURING;

	return 0;
}


int8_t Decision_making()
{
	switch(E_GAS_STATES){
	case ST_START_MEASURING:
		DEBUG_SIMPLE("\n START MEASUREMENT ", NEWLINE_FALSE);

		start_measurement();

		Decision_change(ST_MEASURING);
		break;

	case ST_MEASURING:

		// Wait for the measurement to timeout
		wait_for_stopwatch(MEASUREMENT_TIMEOUT_US);

		DEBUG_SIMPLE("TIMEOUT", NEWLINE_TRUE);
		stop_measurement();

		Decision_change(ST_ANALYSE_MEASUREMENTS);
		break;

	case ST_ANALYSE_MEASUREMENTS:
		values_to_send[0] = (uint16_t)(analyse_measurements()*10);
		values_to_send[1] = (uint16_t)(adc_get_temp()*10);
		values_to_send[2]; // battery voltage

		Decision_change(ST_START_SENDING);
		break;

	case ST_SLEEPING:
		go_to_sleep();

		Decision_change(ST_START_MEASURING);
		break;

	case ST_DEEP_SLEEPING:
		go_to_deep_sleep();

		Decision_change(ST_START_MEASURING);
		break;

	case ST_START_SENDING:
		// turn on bluetooth.
		init_bluetooth();

		update_packet_to_send(&values_to_send[0], MAX_READINGS_IN_MESSAGE);

		// turn on bluetooth
		start_sending();

		Decision_change(ST_BT_SENDING);
		break;

	case ST_BT_SENDING:
		// Wait for all the packets to send
		if(bluetooth_done) {
			bluetooth_done = 0;
			stop_sending();

			Decision_change(ST_DEEP_SLEEPING);
		} else {
			EMU_EnterEM1();
		}
		break;
	default:
		break;
	}

	return 0;
}

int8_t Decision_change(E_GAS_STATES_t input)
{


	//if (measuring_in_progress() == 0)
	//{ // measurement is not in progress
		E_GAS_STATES = input;
		return 0;
	//}
	//return -1;
}

